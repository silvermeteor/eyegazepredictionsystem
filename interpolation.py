import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
import numpy as np
import json


def pointavg (data, scope) :
    N = 0
    sum_x = 0
    sum_y = 0
    for point in data :
        if point[0] > scope['minX'] and point[0] < scope['maxX'] and point[1] > scope['minY'] and point[1] < scope['maxY'] :
            sum_x += point[0]
            sum_y += point[1]
            N += 1

    avg_x = int(sum_x / N)
    avg_y = int(sum_y / N)
    return [avg_x, avg_y]

def pointmedian (data, scope) :
    _data_x = []
    _data_y = []
    for point in data :
        if point[0] > scope['minX'] and point[0] < scope['maxX'] and point[1] > scope['minY'] and point[1] < scope['maxY'] :
            _data_x.append(point[0])
            _data_y.append(point[1])
    median_x = statistics.median(_data_x)
    median_y = statistics.median(_data_y)
    return [median_x, median_y]


def rev_calc_corner (center, q_avg, corner) :
    distance_avg_x = q_avg[0] - center[0]
    distance_corner_x = corner[0] - center[0]
    ratio_x = distance_corner_x / distance_avg_x

    distance_avg_y = q_avg[1] - center[1]
    distance_corner_y = corner[1] - center[1]
    ratio_y = distance_corner_y / distance_avg_y

    return [round(ratio_x, 2), round(ratio_y, 2)]

def calc_corner (center, q_avg, param) :
    distance_avg_x = q_avg[0] - center[0]
    corner_x = center[0] + (distance_avg_x * param[0])

    distance_avg_y = q_avg[1] - center[1]
    corner_y = center[1] + (distance_avg_y * param[1])
    return [int(corner_x), int(corner_y)]


def mapping (x, y, ref_corner):
    point = interpolate(ref_corner, x, y)
    web_screen = [1280, 700]
    return [ int(web_screen[0]*point[0]) , int(web_screen[1]*point[1]) ]


def interpolate (corner, x, y):
    A=np.array([[1, 0, 0, 0], [1, 1, 0, 0], [1, 1, 1, 1], [1, 0, 1, 0]])
    AI = np.linalg.inv(A)

    px = np.array([[corner[0][0], corner[1][0], corner[2][0], corner[3][0]]])
    py = np.array([[corner[0][1], corner[1][1], corner[2][1], corner[3][1]]])

    a = np.matmul(AI,px.transpose())
    b = np.matmul(AI,py.transpose())

    y_ = mapping2(x,y,a,b)

    px = np.array([[corner[0][1], corner[3][1], corner[2][1], corner[1][1]]])
    py = np.array([[corner[0][0], corner[3][0], corner[2][0], corner[1][0]]])

    a = np.matmul(AI,px.transpose())
    b = np.matmul(AI,py.transpose())

    x_ = mapping2(y,x,a,b)

    return [x_, y_]

def mapping2(x,y,a,b):
    aa = a[3]*b[2] - a[2]*b[3]
    bb = a[3]*b[0] -a[0]*b[3] + a[1]*b[2] - a[2]*b[1] + x*b[3] - y*a[3]
    cc = a[1]*b[0] -a[0]*b[1] + x*b[1] - y*a[1]

    det = np.sqrt(bb*bb - 4*aa*cc)
    m = (-bb+det)/(2*aa)

    return m

def serialize_txt (_list) :
    txt = ""
    for data in _list :
        if txt != "" :
            txt += ","
        txt += str(data)
    return txt

def w_average (_list) :
    list_len = len(_list)
    sum_ = 0
    N = 0
    for i in _list :
        sum_ += i*list_len
        N += list_len
        list_len -= 1
    return int(sum_ / N)

def calc_refcorner (x_, y_) :
    a_x = w_average([x_[0], x_[1], x_[2], x_[3]]) - 60
    a_y = w_average([y_[0], y_[4], y_[8], y_[12], y_[16]]) - 60

    b_x = w_average([x_[16], x_[17], x_[18], x_[19]]) + 60
    b_y = w_average([y_[16], y_[12], y_[8], y_[4], y_[0]]) - 60

    c_x = w_average([x_[19], x_[18], x_[17], x_[16]]) + 60
    c_y = w_average([y_[19], y_[15], y_[11], y_[7], y_[3]]) + 60

    d_x = w_average([x_[3], x_[2], x_[1], x_[0]]) - 60
    d_y = w_average([y_[3], y_[7], y_[11], y_[15], y_[19]]) + 60

    return [[a_x, a_y], [b_x, b_y], [c_x, c_y], [d_x, d_y]]


def findAvgDiff (origin, compare) :
    if len(origin) == len(compare) :
        sum_ = 0
        N = 0
        for i in range(len(origin)) :
            diff = np.sqrt( (origin[i][0]-compare[i][0])*(origin[i][0]-compare[i][0]) +  (origin[i][1]-compare[i][1])*(origin[i][1]-compare[i][1]))
            sum_ += diff
            N += 1
        return sum_ / N
    else :
        return False       

def findDiffList (origin, compare, mode='xy') :
    if len(origin) == len(compare) :
        difflist = []
        for i in range(len(origin)) :
            diff = 0
            if mode == 'xy' :
                diff = np.sqrt( (origin[i][0]-compare[i][0])*(origin[i][0]-compare[i][0]) +  (origin[i][1]-compare[i][1])*(origin[i][1]-compare[i][1]))
            elif mode == 'x' :
                diff =  origin[i][0]-compare[i][0]
            elif mode == 'y' :
                diff =  origin[i][1]-compare[i][1]
            elif mode == 'x-abs' :
                diff =  abs(origin[i][0]-compare[i][0])
            elif mode == 'y-abs' :
                diff =  abs(origin[i][1]-compare[i][1])
            difflist.append(diff)
        return difflist
    else :
        return False            
      

ref_corner = [[-51, -205], [1148, -191], [1166, 580], [-56, 499]]

RFL_Positions = [[60,60],[60,253],[60,446],[60,639],[350,60],[350,253],[350,446],[350,639],[640,60],[640,253],[640,446],[640,639],[930,60],[930,253],[930,446],[930,639],[1220,58],[1220,253],[1220,446],[1220,639]]


#f = open("csv3/ttest_before_after--.csv", "w")
#f2 = open("csv3/ttest-y_all.csv", "w")

#f.write("Case,Mean Before,Mean After\n")
#f2.write("Case,Mean Before,Mean After\n")
for _i in range(15,20) :







    p = 'p'+str(_i)

    json_raw = open('results/'+p+'.json', 'r').read()
    json_raw_arr = json.loads(json_raw)

    plt.rcParams["figure.figsize"] = (7,5)
    plt.figure()
    plt.title("Case #"+str(_i+1)+"")

    json_arr = json_raw_arr['gaze_result']

    #data_tobii = json_arr['tobii']
    #_data_tobii = []

    params_ir = [[3.24, 5.31], [3.52, 4.4], [3.91, 5.37], [3.81, 5.09]] #ir
    #params_wc = [[4.08, 5.52], [3.71, 4.89], [4.28, 5.19], [4.04, 6.51]] #webcam
    params_wc = [[3.7, 4.79], [4.21, 4.43], [4.74, 4.61], [4.96, 4.17]] #webcam

    start = 13
    span = 7
    duration = 20

    '''
    for i, rec in enumerate(data_tobii) :
        if i > (len(data_tobii)/duration) * start and i < ((len(data_tobii)/duration) * start) + span*(len(data_tobii)/duration) :
            _data_tobii.append(rec)
    '''

    data_webcam = json_arr['webcam']
    _data_webcam = []

    for i, rec in enumerate(data_webcam) :
        if i > (len(data_webcam)/duration) * start and i < ((len(data_webcam)/duration) * start) + span*(len(data_webcam)/duration) :
            _data_webcam.append(rec)

    data4 = np.array(_data_webcam)
    x, y = data4.T

    data_frame = _data_webcam
    center = pointavg(data_frame, {'minX':-9999 , 'maxX':9999 ,'minY':-9999 ,'maxY':9999 })
    q1 = pointavg(data_frame, {'minX':-9999 , 'maxX':center[0] ,'minY':-9999 ,'maxY':center[1] })
    q2 = pointavg(data_frame, {'minX':center[0] , 'maxX':9999 ,'minY':-9999 ,'maxY':center[1] })
    q3 = pointavg(data_frame, {'minX':-9999 , 'maxX':center[0] ,'minY':center[1] ,'maxY':9999 })
    q4 = pointavg(data_frame, {'minX':center[0] , 'maxX':9999 ,'minY':center[1] ,'maxY':9999 })

    corner_q1 = calc_corner (center, q1, params_wc[0])
    corner_q2 = calc_corner (center, q2, params_wc[1])
    corner_q3 = calc_corner (center, q3, params_wc[2])
    corner_q4 = calc_corner (center, q4, params_wc[3])

    ref_corner_wc = [corner_q1, corner_q2, corner_q4, corner_q3]
    ref_corner_wc_x, ref_corner_wc_y = np.array(ref_corner_wc).T

    #interpolation
    fixed_points = []
    for rec in _data_webcam : 
        fixed = mapping(rec[0], rec[1], ref_corner_wc)
        fixed_points.append(fixed)
    fixed_points_ = np.array(fixed_points)    
    fixed_x, fixed_y = fixed_points_.T
    
    plt.xlim([-300, 1600])
    plt.ylim([-350, 1000])
    plt.gca().invert_yaxis()
    plt.plot(x, y, 'o', color='orangered', ms=1, label="Before")
    plt.plot(fixed_x, fixed_y, 'o', color='limegreen', ms=1, label="After")


    #plt.plot(ref_corner_ir_x, ref_corner_ir_y, 'x', color='pink', ms=8)
    #plt.plot(ref_corner_wc_x, ref_corner_wc_y, 'x', color='limegreen', ms=8)

    plt.gca().add_patch(Rectangle((0,0),1280,700,linewidth=1,edgecolor='r',facecolor='none'))
    plt.savefig("results/interpolation/Case"+str(_i+1)+"_0_density.png")








    plt.rcParams["figure.figsize"] = (7,5)
    plt.figure()
    plt.title("Case #"+str(_i+1)+" - RFL test")
    json_arr = json_raw_arr['rfl_post']
    result = {}
    for rec in json_arr:
        key = rec[0]
        ir_x = rec[2][0]
        ir_y = rec[2][1]
        wc_x = rec[1][0]
        wc_y = rec[1][1]

        fixed = mapping(wc_x, wc_y, ref_corner_wc)

        if((key in result) == False):
            result[key] = {'ir_x': [], 'ir_y':[], 'wc_x':[], 'wc_y':[], 'wc2_x':[], 'wc2_y':[]}
        
        result[key]['ir_x'].append(ir_x)
        result[key]['ir_y'].append(ir_y)
        result[key]['wc_x'].append(wc_x)
        result[key]['wc_y'].append(wc_y)
        result[key]['wc2_x'].append(fixed[0])
        result[key]['wc2_y'].append(fixed[1])

    ir_list = []
    wc_list = []
    wc2_list = []

    result_avg = {}

    for key, rec in result.items():
        ir = [np.average(rec['ir_x']), np.average(rec['ir_y'])]
        wc = [np.average(rec['wc_x']), np.average(rec['wc_y'])]
        wc2 = [np.average(rec['wc2_x']), np.average(rec['wc2_y'])]
        ir_list.append(ir)
        wc_list.append(wc)
        wc2_list.append(wc2)

    ir_x_list, ir_y_list = np.array(ir_list).T
    wc_x_list, wc_y_list = np.array(wc_list).T
    wc2_x_list, wc2_y_list = np.array(wc2_list).T

    rfl_pos = np.array(RFL_Positions)
    rfl_x, rfl_y = rfl_pos.T

    ir_corner = calc_refcorner (ir_x_list, ir_y_list) 
    ir_corner_x, ir_corner_y = np.array(ir_corner).T
    wc_corner = calc_refcorner (wc_x_list, wc_y_list)
    wc_corner_x, wc_corner_y = np.array(wc_corner).T 

    plt.xlim([-300, 1600])
    plt.ylim([-350, 1000])
    plt.gca().invert_yaxis()
    plt.plot(rfl_x, rfl_y, 'x', color='black', ms=5)
    #plt.plot(ir_x_list, ir_y_list, 'o', color='limegreen', ms=3)
    plt.plot(wc_x_list, wc_y_list, 'o', color='orangered', ms=3, label="Before")
    plt.plot(wc2_x_list, wc2_y_list, 'o', color='limegreen', ms=3, label="After")

    #plt.plot(ir_corner_x, ir_corner_y, 'x', color='pink', ms=8)
    #plt.plot(wc_corner_x, wc_corner_y, 'x', color='limegreen', ms=8)

    #for i, rec in enumerate(ir_list) :
    #    plt.text(rec[0]+.04, rec[1]+.04, "p"+str(i), fontsize=8, color='limegreen')
    for i, rec in enumerate(wc_list) :
        plt.text(rec[0]+.04, rec[1]+.04, "p"+str(i), fontsize=8, color='orangered')
    for i, rec in enumerate(wc2_list) :
        plt.text(rec[0]+.04, rec[1]+.04, "p"+str(i), fontsize=8, color='limegreen')

    plt.gca().add_patch(Rectangle((0,0),1280,700,linewidth=1,edgecolor='r',facecolor='none'))

    #mean_diff_before = findAvgDiff(rfl_pos, wc_list)
    #mean_diff_after = findAvgDiff(rfl_pos, wc2_list)

    '''
    for i in range(len(mean_diff_before_ls)) :
        line2 = str(_i) + ',' + str(round(mean_diff_before_ls[i], 2)) + ',' + str(round(mean_diff_after_ls[i],2)) + "\n"
        f2.write(line2)
    '''        
    '''
    line = str(_i) + ',' + str(round(mean_diff_before, 2)) + ',' + str(round(mean_diff_after,2)) + "\n"
    print(line)
    f.write(line)
    '''
    #f2.write(line)
    plt.legend()
    plt.savefig("results/interpolation/Case"+str(_i+1)+"_1_rfl.png")




    plt.rcParams["figure.figsize"] = (8,4)
    plt.figure()
    mean_diff_before_ls = findDiffList(rfl_pos, wc_list)
    mean_diff_after_ls = findDiffList(rfl_pos, wc2_list)

    _p = []
    for i in range(0,20):
        _p.append('p'+str(i))

    barWidth = 0.35
    br1 = np.arange(len(_p))
    br2 = [x + barWidth for x in br1]

    plt.title("Case #"+str(_i+1)+"")
    plt.bar(br1, mean_diff_before_ls, 0.35, label='Before', color="orangered")
    plt.bar(br2, mean_diff_after_ls, 0.35, label='After', color="limegreen")
    plt.xticks([r + barWidth for r in range(len(_p))], _p)
    plt.legend()
    plt.ylabel('Deviation from target', fontsize=16)
    plt.savefig("results/interpolation/Case"+str(_i+1)+"_2_bar.png")





    plt.rcParams["figure.figsize"] = (4,4)
    plt.figure()
    plt.title("Case #"+str(_i+1)+"")
    boxplot_data = [mean_diff_before_ls, mean_diff_after_ls]
    plt.boxplot(boxplot_data)
    plt.xticks([1,2],["Before", "After"])
    #plt.ylabel('Deviation from target', fontsize=16)
    plt.savefig("results/interpolation/Case"+str(_i+1)+"_3_box.png")
    




    plt.rcParams["figure.figsize"] = (8,4)
    plt.figure()
    mean_diff_before_x_ls = findDiffList(rfl_pos, wc_list, 'x-abs')
    mean_diff_after_x_ls = findDiffList(rfl_pos, wc2_list, 'x-abs')

    _p = []
    for i in range(0,20):
        _p.append('p'+str(i))

    barWidth = 0.35
    br1 = np.arange(len(_p))
    br2 = [x + barWidth for x in br1]

    plt.title("Case #"+str(_i+1)+" (X-axis)")
    plt.bar(br1, mean_diff_before_x_ls, 0.35, label='Before', color="orangered")
    plt.bar(br2, mean_diff_after_x_ls, 0.35, label='After', color="limegreen")
    plt.xticks([r + barWidth for r in range(len(_p))], _p)
    plt.legend()
    plt.ylabel('Deviation from target', fontsize=16)
    plt.savefig("results/interpolation/Case"+str(_i+1)+"_4_barx.png")





    plt.rcParams["figure.figsize"] = (4,4)
    plt.figure()
    plt.title("Case #"+str(_i+1)+" (X-axis)")
    boxplot_data = [mean_diff_before_x_ls, mean_diff_after_x_ls]
    plt.boxplot(boxplot_data)
    plt.xticks([1,2],["Before", "After"])
    #plt.ylabel('Deviation from target', fontsize=16)
    plt.savefig("results/interpolation/Case"+str(_i+1)+"_5_boxx.png")






    plt.rcParams["figure.figsize"] = (8,4)
    plt.figure()
    mean_diff_before_y_ls = findDiffList(rfl_pos, wc_list, 'y-abs')
    mean_diff_after_y_ls = findDiffList(rfl_pos, wc2_list, 'y-abs')

    _p = []
    for i in range(0,20):
        _p.append('p'+str(i))

    barWidth = 0.35
    br1 = np.arange(len(_p))
    br2 = [x + barWidth for x in br1]

    plt.title("Case #"+str(_i+1)+" (Y-axis)")
    plt.bar(br1, mean_diff_before_y_ls, 0.35, label='Before', color="orangered")
    plt.bar(br2, mean_diff_after_y_ls, 0.35, label='After', color="limegreen")
    plt.xticks([r + barWidth for r in range(len(_p))], _p)
    plt.legend()
    plt.ylabel('Deviation from target', fontsize=16)
    plt.savefig("results/interpolation/Case"+str(_i+1)+"_6_bary.png")





    plt.rcParams["figure.figsize"] = (4,4)
    plt.figure()
    plt.title("Case #"+str(_i+1)+" (Y-axis)")
    boxplot_data = [mean_diff_before_y_ls, mean_diff_after_y_ls]
    plt.boxplot(boxplot_data)
    plt.xticks([1,2],["Before", "After"])
    #plt.ylabel('Deviation from target', fontsize=16)
    plt.savefig("results/interpolation/Case"+str(_i+1)+"_7_boxy.png")




    #plt.show()

#f.close()
