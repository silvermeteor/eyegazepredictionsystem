//old scripts
function binarize (source, dest, threshold){
		canvas_source = document.getElementById(source);
		context_source = canvas_source.getContext("2d");

		canvas_dest = document.getElementById(dest);
		context_dest = canvas_dest.getContext("2d");
		
		var _w = $('#'+source).width();
		var _h = $('#'+source).height();
		
		imageData = context_source.getImageData(0, 0, _w, _h);
		
		for (y = 0; y < _h; y++) {
			inpos = y * _w * 4; 
			outpos = inpos;
			for (x = 0; x < _w ; x++) {
			// Get the pixel of the red channel.
				r = imageData.data[inpos++]
			// Get the pixel of the green channel.
				g = imageData.data[inpos++]
			// Get the pixel of the blue channel.
				b = imageData.data[inpos++]
			// Get the pixel of the alpha channel.
				a = imageData.data[inpos++]
				// Transform RGB color space to gray scale.
				gray =  (0.299 * r + 0.587 * g + 0.114 * b)
				// This is our threshold. You can change it.
				if ( gray > threshold )
				{
				// Set the pixel is white.
					imageData.data[outpos++] = 255;
					imageData.data[outpos++] = 255;
					imageData.data[outpos++] = 255;
					imageData.data[outpos++] = a;
				}
				else
				{
				// Set the pixel is black.
					imageData.data[outpos++] = 0;
					imageData.data[outpos++] = 0;
					imageData.data[outpos++] = 0;
					imageData.data[outpos++] = a;
				}
			} 
			//console.log('y = '+y);
		} 
		context_dest.putImageData(imageData, 0, 0);
	}
	
	function getBinary (id, threshold){
		var binary = [];
		var pos = 0;
		
		var _w = $('#'+id).width();
		var _h = $('#'+id).height();
		
		canvas_source = document.getElementById(id);
		context_source = canvas_source.getContext("2d");
		
		imageData = context_source.getImageData(0, 0, _w, _h);
		
		for (y = 0; y < _h; y++) {
			binary[y] = [];
			inpos = y * _w * 4; 
			outpos = inpos;
			for (x = 0; x < _w; x++) {
			// Get the pixel of the red channel.
				r = imageData.data[inpos++]
			// Get the pixel of the green channel.
				g = imageData.data[inpos++]
			// Get the pixel of the blue channel.
				b = imageData.data[inpos++]
			// Get the pixel of the alpha channel.
				a = imageData.data[inpos++]
				// Transform RGB color space to gray scale.
				gray =  (0.299 * r + 0.587 * g + 0.114 * b)
				// This is our threshold. You can change it.
				if ( gray > threshold )
				{
				// Set the pixel is white.
					imageData.data[outpos++] = 255;
					imageData.data[outpos++] = 255;
					imageData.data[outpos++] = 255;
					imageData.data[outpos++] = a;
					binary[y][x] = 1;
				}
				else
				{
				// Set the pixel is black.
					imageData.data[outpos++] = 0;
					imageData.data[outpos++] = 0;
					imageData.data[outpos++] = 0;
					imageData.data[outpos++] = a;
					binary[y][x] = 0;
				}
			} 
		} 
		//context_s.putImageData(imageData, 0, 0);
		//context_b.drawImage(canvas_s, 0, 0, 640, 480);
		return binary;
	}
	
	function getMask (size){
		var binary = [];
		var pos = 0;
		context_mask.canvas.width = size;
		context_mask.canvas.height = size;
		var mask = document.getElementById('mask');
		context_mask.drawImage(mask, 0, 0, size, size);
		imageData = context_mask.getImageData(0, 0, size, size);
		for (y = 0; y < size; y++) {
			binary[y] = [];
			inpos = y * size * 4; 
			outpos = inpos;
			for (x = 0; x < size; x++) {
			// Get the pixel of the red channel.
				r = imageData.data[inpos++]
			// Get the pixel of the green channel.
				g = imageData.data[inpos++]
			// Get the pixel of the blue channel.
				b = imageData.data[inpos++]
			// Get the pixel of the alpha channel.
				a = imageData.data[inpos++]
				// Transform RGB color space to gray scale.
				gray =  (0.299 * r + 0.587 * g + 0.114 * b)
				// This is our threshold. You can change it.
				if ( gray > 120 )
				{
				// Set the pixel is white.
					imageData.data[outpos++] = 255;
					imageData.data[outpos++] = 255;
					imageData.data[outpos++] = 255;
					imageData.data[outpos++] = a;
					binary[y][x] = 1;
				}
				else
				{
				// Set the pixel is black.
					imageData.data[outpos++] = 0;
					imageData.data[outpos++] = 0;
					imageData.data[outpos++] = 0;
					imageData.data[outpos++] = a;
					binary[y][x] = 0;
				}
			} 
		} 
		context_mask.putImageData(imageData, 0, 0);
		return binary;
	}
	
	function compareBinary (array1, array2, size){
		var rawscore = 0;
		for(var i=0; i<size; i++){
			for(var j=0; j<size; j++){
				if(array1[i][j] == array2[i][j]){
					rawscore++;
				}
			}
		}
		return parseInt(100 * rawscore / (size * size));
	}
	
	function compareMask (pic, mask, x, y){
		var pic_h = pic.length;
		var pic_w = pic[0].length;
		var mask_h = mask.length;
		var mask_w = mask[0].length;
		var rawscore = 0;
		
		if(pic_w - x >= mask_w && pic_h - y >= mask_h){
			for(var i=0; i<mask_h; i++){
				for(var j=0; j<mask_w; j++){
					/*if(mask[i][j] == pic[i+y][j+x]){
						rawscore++;
					}*/
					if( (mask[i][j] == 1 && pic[i+y][j+x] == 1) || (mask[i][j] == 0 && pic[i+y][j+x] == 0)){
						rawscore++;
					}else{
						//rawscore--;
					}
				}
			}
		}
		
		var result = parseInt(100 * rawscore / (mask_h * mask_w));
		
		if(result > 70){
			//show frame
			var frame = $('<div class="frame"></div>');
			$('body').append(frame);
			var _baseX = $('#canvas_s').offset().left;
			var _baseY = $('#canvas_s').offset().top;
			$(frame).css({
				position: 'absolute',
				top: _baseY + y,
				left: _baseX + x
			});
		}
		
		return result;
	}
	
	function scan(){
		for(var y = 1; y < 40; y++ ){
			for(var x = 1; x < 65; x++ ){
				compareMask(pic, mask1, x, y);
			}
		}
	}
	