
function interpolate (corner, x, y){
    var A=[[1, 0, 0, 0], [1, 1, 0, 0], [1, 1, 1, 1], [1, 0, 1, 0]];
    var AI = inv(A);

    var px = [[corner[0][0], corner[1][0], corner[2][0], corner[3][0]]];
    var py = [[corner[0][1], corner[1][1], corner[2][1], corner[3][1]]];

    var a = mulmat(AI,transpose(px));
    var b = mulmat(AI,transpose(py));

    var y_ = mapping(x,y,a,b);


    px = [[corner[0][1], corner[3][1], corner[2][1], corner[1][1]]];
    py = [[corner[0][0], corner[3][0], corner[2][0], corner[1][0]]];

    a = mulmat(AI,transpose(px));
    b = mulmat(AI,transpose(py));

    var x_ = mapping2(y,x,a,b);

    return [x_, y_];
}

function mapping2(x,y,a,b) {
    aa = a[3]*b[2] - a[2]*b[3];
    bb = a[3]*b[0] -a[0]*b[3] + a[1]*b[2] - a[2]*b[1] + x*b[3] - y*a[3];
    cc = a[1]*b[0] -a[0]*b[1] + x*b[1] - y*a[1];

    det = Math.sqrt(bb*bb - 4*aa*cc);
    m = (-bb+det)/(2*aa);

    //l = (x-a[0]-a[2]*m)/(a[1]+a[3]*m);

    return m;
}


function inv(_A) {
    var temp,
    N = _A.length,
    E = [];
   
    for (var i = 0; i < N; i++)
      E[i] = [];
   
    for (i = 0; i < N; i++)
      for (var j = 0; j < N; j++) {
        E[i][j] = 0;
        if (i == j)
          E[i][j] = 1;
      }
   
    for (var k = 0; k < N; k++) {
      temp = _A[k][k];
   
      for (var j = 0; j < N; j++)
      {
        _A[k][j] /= temp;
        E[k][j] /= temp;
      }
   
      for (var i = k + 1; i < N; i++)
      {
        temp = _A[i][k];
   
        for (var j = 0; j < N; j++)
        {
          _A[i][j] -= _A[k][j] * temp;
          E[i][j] -= E[k][j] * temp;
        }
      }
    }
   
    for (var k = N - 1; k > 0; k--)
    {
      for (var i = k - 1; i >= 0; i--)
      {
        temp = _A[i][k];
   
        for (var j = 0; j < N; j++)
        {
          _A[i][j] -= _A[k][j] * temp;
          E[i][j] -= E[k][j] * temp;
        }
      }
    }
   
    for (var i = 0; i < N; i++)
      for (var j = 0; j < N; j++)
        _A[i][j] = E[i][j];
    return _A;
  }

function transpose(matrix) {
    const rows = matrix.length, cols = matrix[0].length;
    const grid = [];
    for (let j = 0; j < cols; j++) {
        grid[j] = Array(rows);
    }
    for (let i = 0; i < rows; i++) {
        for (let j = 0; j < cols; j++) {
        grid[j][i] = matrix[i][j];
        }
    }
    return grid;
}

function mulmat(m1, m2) {
    var result = [];
    for (var i = 0; i < m1.length; i++) {
        result[i] = [];
        for (var j = 0; j < m2[0].length; j++) {
            var sum = 0;
            for (var k = 0; k < m1[0].length; k++) {
                sum += m1[i][k] * m2[k][j];
            }
            result[i][j] = sum;
        }
    }
    return result;
}