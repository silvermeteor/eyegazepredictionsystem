import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
import numpy as np
import json


def pointavg (data, scope) :
    N = 0
    sum_x = 0
    sum_y = 0
    for point in data :
        if point[0] > scope['minX'] and point[0] < scope['maxX'] and point[1] > scope['minY'] and point[1] < scope['maxY'] :
            sum_x += point[0]
            sum_y += point[1]
            N += 1

    avg_x = int(sum_x / N)
    avg_y = int(sum_y / N)
    return [avg_x, avg_y]

def pointmedian (data, scope) :
    _data_x = []
    _data_y = []
    for point in data :
        if point[0] > scope['minX'] and point[0] < scope['maxX'] and point[1] > scope['minY'] and point[1] < scope['maxY'] :
            _data_x.append(point[0])
            _data_y.append(point[1])
    median_x = statistics.median(_data_x)
    median_y = statistics.median(_data_y)
    return [median_x, median_y]


def rev_calc_corner (center, q_avg, corner) :
    distance_avg_x = q_avg[0] - center[0]
    distance_corner_x = corner[0] - center[0]
    ratio_x = distance_corner_x / distance_avg_x

    distance_avg_y = q_avg[1] - center[1]
    distance_corner_y = corner[1] - center[1]
    ratio_y = distance_corner_y / distance_avg_y

    return [round(ratio_x, 2), round(ratio_y, 2)]

def calc_corner (center, q_avg, param) :
    distance_avg_x = q_avg[0] - center[0]
    corner_x = center[0] + (distance_avg_x * param[0])

    distance_avg_y = q_avg[1] - center[1]
    corner_y = center[1] + (distance_avg_y * param[1])
    return [int(corner_x), int(corner_y)]


def mapping (x, y, ref_corner):
    point = interpolate(ref_corner, x, y)
    web_screen = [1280, 700]
    return [ int(web_screen[0]*point[0]) , int(web_screen[1]*point[1]) ]


def interpolate (corner, x, y):
    A=np.array([[1, 0, 0, 0], [1, 1, 0, 0], [1, 1, 1, 1], [1, 0, 1, 0]])
    AI = np.linalg.inv(A)

    px = np.array([[corner[0][0], corner[1][0], corner[2][0], corner[3][0]]])
    py = np.array([[corner[0][1], corner[1][1], corner[2][1], corner[3][1]]])

    a = np.matmul(AI,px.transpose())
    b = np.matmul(AI,py.transpose())

    y_ = mapping2(x,y,a,b)

    px = np.array([[corner[0][1], corner[3][1], corner[2][1], corner[1][1]]])
    py = np.array([[corner[0][0], corner[3][0], corner[2][0], corner[1][0]]])

    a = np.matmul(AI,px.transpose())
    b = np.matmul(AI,py.transpose())

    x_ = mapping2(y,x,a,b)

    return [x_, y_]

def mapping2(x,y,a,b):
    aa = a[3]*b[2] - a[2]*b[3]
    bb = a[3]*b[0] -a[0]*b[3] + a[1]*b[2] - a[2]*b[1] + x*b[3] - y*a[3]
    cc = a[1]*b[0] -a[0]*b[1] + x*b[1] - y*a[1]

    det = np.sqrt(bb*bb - 4*aa*cc)
    m = (-bb+det)/(2*aa)

    return m

def serialize_txt (_list) :
    txt = ""
    for data in _list :
        if txt != "" :
            txt += ","
        txt += str(data)
    return txt

def w_average (_list) :
    list_len = len(_list)
    sum_ = 0
    N = 0
    for i in _list :
        sum_ += i*list_len
        N += list_len
        list_len -= 1
    return int(sum_ / N)

def calc_refcorner (x_, y_) :
    a_x = w_average([x_[0], x_[1], x_[2], x_[3]]) - 60
    a_y = w_average([y_[0], y_[4], y_[8], y_[12], y_[16]]) - 60

    b_x = w_average([x_[16], x_[17], x_[18], x_[19]]) + 60
    b_y = w_average([y_[16], y_[12], y_[8], y_[4], y_[0]]) - 60

    c_x = w_average([x_[19], x_[18], x_[17], x_[16]]) + 60
    c_y = w_average([y_[19], y_[15], y_[11], y_[7], y_[3]]) + 60

    d_x = w_average([x_[3], x_[2], x_[1], x_[0]]) - 60
    d_y = w_average([y_[3], y_[7], y_[11], y_[15], y_[19]]) + 60

    return [[a_x, a_y], [b_x, b_y], [c_x, c_y], [d_x, d_y]]

def findDiffList (origin, compare, mode='xy') :
    if len(origin) == len(compare) :
        difflist = []
        for i in range(len(origin)) :
            diff = 0
            if mode == 'xy' :
                diff = np.sqrt( (origin[i][0]-compare[i][0])*(origin[i][0]-compare[i][0]) +  (origin[i][1]-compare[i][1])*(origin[i][1]-compare[i][1]))
            elif mode == 'x' :
                diff =  origin[i][0]-compare[i][0]
            elif mode == 'y' :
                diff =  origin[i][1]-compare[i][1]
            difflist.append(diff)
        return difflist
    else :
        return False            


def findAvgDiff (origin, compare) :
    if len(origin) == len(compare) :
        sum_ = 0
        N = 0
        for i in range(len(origin)) :
            diff = np.sqrt( (origin[i][0]-compare[i][0])*(origin[i][0]-compare[i][0]) +  (origin[i][1]-compare[i][1])*(origin[i][1]-compare[i][1]))
            sum_ += diff
            N += 1
        return sum_ / N
    else :
        return False       


ref_corner = [[-51, -205], [1148, -191], [1166, 580], [-56, 499]]

RFL_Positions = [[60,60],[60,253],[60,446],[60,639],[350,60],[350,253],[350,446],[350,639],[640,60],[640,253],[640,446],[640,639],[930,60],[930,253],[930,446],[930,639],[1220,58],[1220,253],[1220,446],[1220,639]]

#f = open("csv3/ttest-syserror.csv", "w")
#f.write("Case,point,IR Before_x,After_x,Before_y,After_y,X-sort,Y-sort\n")

for _i in range(0,20) :



    p = 'p'+str(_i)


    json_raw = open('results/'+p+'.json', 'r').read()
    json_raw_arr = json.loads(json_raw)






    json_arr = json_raw_arr['rfl_pre']
    result = {}
    for rec in json_arr:
        key = rec[0]
        ir_x = rec[2][0]
        ir_y = rec[2][1]
        wc_x = rec[1][0]
        wc_y = rec[1][1]

        #fixed = mapping(wc_x, wc_y, ref_corner)

        if((key in result) == False):
            result[key] = {'ir_x': [], 'ir_y':[], 'wc_x':[], 'wc_y':[], 'wc2_x':[], 'wc2_y':[]}
        
        result[key]['ir_x'].append(ir_x)
        result[key]['ir_y'].append(ir_y)
        result[key]['wc_x'].append(wc_x)
        result[key]['wc_y'].append(wc_y)
        #result[key]['wc2_x'].append(fixed[0])
        #result[key]['wc2_y'].append(fixed[1])

    ir_list_b = []
    wc_list_b = []

    result_avg = {}

    for key, rec in result.items():
        ir = [np.average(rec['ir_x']), np.average(rec['ir_y'])]
        wc = [np.average(rec['wc_x']), np.average(rec['wc_y'])]
        ir_list_b.append(ir)
        wc_list_b.append(wc)

 

    json_arr = json_raw_arr['rfl_post']
    result = {}
    for rec in json_arr:
        key = rec[0]
        ir_x = rec[2][0]
        ir_y = rec[2][1]
        wc_x = rec[1][0]
        wc_y = rec[1][1]

        #fixed = mapping(wc_x, wc_y, ref_corner)

        if((key in result) == False):
            result[key] = {'ir_x': [], 'ir_y':[], 'wc_x':[], 'wc_y':[], 'wc2_x':[], 'wc2_y':[]}
        
        result[key]['ir_x'].append(ir_x)
        result[key]['ir_y'].append(ir_y)
        result[key]['wc_x'].append(wc_x)
        result[key]['wc_y'].append(wc_y)
        #result[key]['wc2_x'].append(fixed[0])
        #result[key]['wc2_y'].append(fixed[1])

    ir_list_a = []
    wc_list_a = []

    result_avg = {}

    for key, rec in result.items():
        ir = [np.average(rec['ir_x']), np.average(rec['ir_y'])]
        wc = [np.average(rec['wc_x']), np.average(rec['wc_y'])]
        ir_list_a.append(ir)
        wc_list_a.append(wc)

    rfl_pos = np.array(RFL_Positions)
    diff_wc_b_x = findDiffList(rfl_pos, wc_list_b, 'x')
    diff_wc_a_x = findDiffList(rfl_pos, wc_list_a, 'x')
    diff_wc_b_y = findDiffList(rfl_pos, wc_list_b, 'y')
    diff_wc_a_y = findDiffList(rfl_pos, wc_list_a, 'y')

    '''
    y_sort = 0
    for i in range(len(diff_wc_b_x)) :
        line = str(_i+1) + ',' + 'p'+str(i) + ',' + str(round(diff_wc_b_x[i], 2)) + ',' + str(round(diff_wc_a_x[i], 2)) + ',' + str(round(diff_wc_b_y[i], 2)) + ',' + str(round(diff_wc_a_y[i], 2))  + ',' + str(i+1) + ',' +str(y_sort)+ "\n"
        if (i+1)%5 == 0 and i>3 :
            y_sort -= 15
        else :
            y_sort += 4
        #f.write(line)

    #f.write(line)
    '''
    #plot x
    plt.rcParams["figure.figsize"] = (10,5)
    plt.figure()

    _p = []
    for i in range(0,20):
        _p.append('p'+str(i))

    barWidth = 0.35
    br1 = np.arange(len(_p))
    br2 = [x + barWidth for x in br1]

    plt.title("Case #"+str(_i+1)+" x-axis")
    plt.bar(br1, diff_wc_b_x, 0.35, label='before 20 min - x')
    plt.bar(br2, diff_wc_a_x, 0.35, label='after 20 min - x')
    plt.xticks([r + barWidth for r in range(len(_p))], _p)
    plt.legend()
    plt.savefig("results/sys_error/Case"+str(_i)+"_x.png")


    #plot y

    #y_sort = [0,4,8,12,16,1,5,9,13,17,2,6,10,14,18,3,7,11,15,19]
    y_sort = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19]
    diff_wc_b_y_ = []
    diff_wc_a_y_ = []
    _p_ = []

    for i in range(0, 20):
        pos = y_sort.index(i)
        diff_wc_b_y_.append(diff_wc_b_y[pos])
        diff_wc_a_y_.append(diff_wc_a_y[pos])
        _p_.append(_p[pos])

    plt.rcParams["figure.figsize"] = (10,5)
    plt.figure()

    plt.title("Case #"+str(_i+1)+" y-axis")
    plt.bar(br1, diff_wc_b_y_, 0.35, label='before 20 min - y')
    plt.bar(br2, diff_wc_a_y_, 0.35, label='after 20 min - y')
    plt.xticks([r + barWidth for r in range(len(_p))], _p_)
    plt.legend()
    plt.savefig("results/sys_error/Case"+str(_i)+"_y.png")
    #plt.show()


#f.close()




