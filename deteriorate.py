import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
import numpy as np
import json


def pointavg (data, scope) :
    N = 0
    sum_x = 0
    sum_y = 0
    for point in data :
        if point[0] > scope['minX'] and point[0] < scope['maxX'] and point[1] > scope['minY'] and point[1] < scope['maxY'] :
            sum_x += point[0]
            sum_y += point[1]
            N += 1

    avg_x = int(sum_x / N)
    avg_y = int(sum_y / N)
    return [avg_x, avg_y]

def pointmedian (data, scope) :
    _data_x = []
    _data_y = []
    for point in data :
        if point[0] > scope['minX'] and point[0] < scope['maxX'] and point[1] > scope['minY'] and point[1] < scope['maxY'] :
            _data_x.append(point[0])
            _data_y.append(point[1])
    median_x = statistics.median(_data_x)
    median_y = statistics.median(_data_y)
    return [median_x, median_y]


def rev_calc_corner (center, q_avg, corner) :
    distance_avg_x = q_avg[0] - center[0]
    distance_corner_x = corner[0] - center[0]
    ratio_x = distance_corner_x / distance_avg_x

    distance_avg_y = q_avg[1] - center[1]
    distance_corner_y = corner[1] - center[1]
    ratio_y = distance_corner_y / distance_avg_y

    return [round(ratio_x, 2), round(ratio_y, 2)]

def calc_corner (center, q_avg, param) :
    distance_avg_x = q_avg[0] - center[0]
    corner_x = center[0] + (distance_avg_x * param[0])

    distance_avg_y = q_avg[1] - center[1]
    corner_y = center[1] + (distance_avg_y * param[1])
    return [int(corner_x), int(corner_y)]


def mapping (x, y, ref_corner):
    point = interpolate(ref_corner, x, y)
    web_screen = [1280, 700]
    return [ int(web_screen[0]*point[0]) , int(web_screen[1]*point[1]) ]


def interpolate (corner, x, y):
    A=np.array([[1, 0, 0, 0], [1, 1, 0, 0], [1, 1, 1, 1], [1, 0, 1, 0]])
    AI = np.linalg.inv(A)

    px = np.array([[corner[0][0], corner[1][0], corner[2][0], corner[3][0]]])
    py = np.array([[corner[0][1], corner[1][1], corner[2][1], corner[3][1]]])

    a = np.matmul(AI,px.transpose())
    b = np.matmul(AI,py.transpose())

    y_ = mapping2(x,y,a,b)

    px = np.array([[corner[0][1], corner[3][1], corner[2][1], corner[1][1]]])
    py = np.array([[corner[0][0], corner[3][0], corner[2][0], corner[1][0]]])

    a = np.matmul(AI,px.transpose())
    b = np.matmul(AI,py.transpose())

    x_ = mapping2(y,x,a,b)

    return [x_, y_]

def mapping2(x,y,a,b):
    aa = a[3]*b[2] - a[2]*b[3]
    bb = a[3]*b[0] -a[0]*b[3] + a[1]*b[2] - a[2]*b[1] + x*b[3] - y*a[3]
    cc = a[1]*b[0] -a[0]*b[1] + x*b[1] - y*a[1]

    det = np.sqrt(bb*bb - 4*aa*cc)
    m = (-bb+det)/(2*aa)

    return m

def serialize_txt (_list) :
    txt = ""
    for data in _list :
        if txt != "" :
            txt += ","
        txt += str(data)
    return txt

def w_average (_list) :
    list_len = len(_list)
    sum_ = 0
    N = 0
    for i in _list :
        sum_ += i*list_len
        N += list_len
        list_len -= 1
    return int(sum_ / N)

def calc_refcorner (x_, y_) :
    a_x = w_average([x_[0], x_[1], x_[2], x_[3]]) - 60
    a_y = w_average([y_[0], y_[4], y_[8], y_[12], y_[16]]) - 60

    b_x = w_average([x_[16], x_[17], x_[18], x_[19]]) + 60
    b_y = w_average([y_[16], y_[12], y_[8], y_[4], y_[0]]) - 60

    c_x = w_average([x_[19], x_[18], x_[17], x_[16]]) + 60
    c_y = w_average([y_[19], y_[15], y_[11], y_[7], y_[3]]) + 60

    d_x = w_average([x_[3], x_[2], x_[1], x_[0]]) - 60
    d_y = w_average([y_[3], y_[7], y_[11], y_[15], y_[19]]) + 60

    return [[a_x, a_y], [b_x, b_y], [c_x, c_y], [d_x, d_y]]

def findDiffList (origin, compare) :
    if len(origin) == len(compare) :
        difflist = []
        for i in range(len(origin)) :
            diff = np.sqrt( (origin[i][0]-compare[i][0])*(origin[i][0]-compare[i][0]) +  (origin[i][1]-compare[i][1])*(origin[i][1]-compare[i][1]))
            difflist.append(diff)
        return difflist
    else :
        return False        


def findAvgDiff (origin, compare) :
    if len(origin) == len(compare) :
        sum_ = 0
        N = 0
        for i in range(len(origin)) :
            diff = np.sqrt( (origin[i][0]-compare[i][0])*(origin[i][0]-compare[i][0]) +  (origin[i][1]-compare[i][1])*(origin[i][1]-compare[i][1]))
            sum_ += diff
            N += 1
        return sum_ / N
    else :
        return False       


ref_corner = [[-51, -205], [1148, -191], [1166, 580], [-56, 499]]

RFL_Positions = [[60,60],[60,253],[60,446],[60,639],[350,60],[350,253],[350,446],[350,639],[640,60],[640,253],[640,446],[640,639],[930,60],[930,253],[930,446],[930,639],[1220,58],[1220,253],[1220,446],[1220,639]]

#f = open("csv3/ttest-deteriorate.csv", "w")
#f.write("Case,IR Before,IR After, WC Before, WC After\n")

for _i in range(0,20) :



    p = 'p'+str(_i)


    json_raw = open('results/'+p+'.json', 'r').read()
    json_raw_arr = json.loads(json_raw)






    json_arr = json_raw_arr['rfl_pre']
    result = {}
    for rec in json_arr:
        key = rec[0]
        ir_x = rec[2][0]
        ir_y = rec[2][1]
        wc_x = rec[1][0]
        wc_y = rec[1][1]

        #fixed = mapping(wc_x, wc_y, ref_corner)

        if((key in result) == False):
            result[key] = {'ir_x': [], 'ir_y':[], 'wc_x':[], 'wc_y':[], 'wc2_x':[], 'wc2_y':[]}
        
        result[key]['ir_x'].append(ir_x)
        result[key]['ir_y'].append(ir_y)
        result[key]['wc_x'].append(wc_x)
        result[key]['wc_y'].append(wc_y)
        #result[key]['wc2_x'].append(fixed[0])
        #result[key]['wc2_y'].append(fixed[1])

    ir_list_b = []
    wc_list_b = []

    result_avg = {}

    for key, rec in result.items():
        ir = [np.average(rec['ir_x']), np.average(rec['ir_y'])]
        wc = [np.average(rec['wc_x']), np.average(rec['wc_y'])]
        ir_list_b.append(ir)
        wc_list_b.append(wc)

    ir_x_list_b, ir_y_list_b = np.array(ir_list_b).T
    wc_x_list_b, wc_y_list_b = np.array(wc_list_b).T

    rfl_pos = np.array(RFL_Positions)
    rfl_x, rfl_y = rfl_pos.T

    ir_corner = calc_refcorner (ir_x_list_b, ir_y_list_b) 
    ir_corner_x, ir_corner_y = np.array(ir_corner).T
    wc_corner = calc_refcorner (wc_x_list_b, wc_y_list_b)
    wc_corner_x, wc_corner_y = np.array(wc_corner).T 

    #plt.xlim([-50, 1400])
    #plt.ylim([-300, 800])
    #plt.gca().invert_yaxis()
    #plt.plot(rfl_x, rfl_y, 'x', color='red', ms=5)
    #plt.plot(ir_x_list, ir_y_list, 'o', color='orange', ms=3)
    #plt.plot(wc_x_list, wc_y_list, 'o', color='blue', ms=3)
    '''
    plt.plot(ir_corner_x, ir_corner_y, 'x', color='pink', ms=8)
    plt.plot(wc_corner_x, wc_corner_y, 'x', color='green', ms=8)
    '''
    '''
    for i, rec in enumerate(ir_list_b) :
        plt.text(rec[0]+.04, rec[1]+.04, "IR"+str(i), fontsize=8, color='orange')
    for i, rec in enumerate(wc_list_b) :
        plt.text(rec[0]+.04, rec[1]+.04, "WC"+str(i), fontsize=8, color='blue')

    plt.gca().add_patch(Rectangle((0,0),1280,700,linewidth=1,edgecolor='r',facecolor='none'))
    '''
    mean_diff_ir_b = findAvgDiff(rfl_pos, ir_list_b)
    mean_diff_wc_b = findAvgDiff(rfl_pos, wc_list_b)








 

    json_arr = json_raw_arr['rfl_post']
    result = {}
    for rec in json_arr:
        key = rec[0]
        ir_x = rec[2][0]
        ir_y = rec[2][1]
        wc_x = rec[1][0]
        wc_y = rec[1][1]

        #fixed = mapping(wc_x, wc_y, ref_corner)

        if((key in result) == False):
            result[key] = {'ir_x': [], 'ir_y':[], 'wc_x':[], 'wc_y':[], 'wc2_x':[], 'wc2_y':[]}
        
        result[key]['ir_x'].append(ir_x)
        result[key]['ir_y'].append(ir_y)
        result[key]['wc_x'].append(wc_x)
        result[key]['wc_y'].append(wc_y)
        #result[key]['wc2_x'].append(fixed[0])
        #result[key]['wc2_y'].append(fixed[1])

    ir_list_a = []
    wc_list_a = []

    result_avg = {}

    for key, rec in result.items():
        ir = [np.average(rec['ir_x']), np.average(rec['ir_y'])]
        wc = [np.average(rec['wc_x']), np.average(rec['wc_y'])]
        ir_list_a.append(ir)
        wc_list_a.append(wc)

    ir_x_list_a, ir_y_list_a = np.array(ir_list_a).T
    wc_x_list_a, wc_y_list_a = np.array(wc_list_a).T

    rfl_pos = np.array(RFL_Positions)
    rfl_x, rfl_y = rfl_pos.T

    ir_corner = calc_refcorner (ir_x_list_a, ir_y_list_a) 
    ir_corner_x, ir_corner_y = np.array(ir_corner).T
    wc_corner = calc_refcorner (wc_x_list_a, wc_y_list_a)
    wc_corner_x, wc_corner_y = np.array(wc_corner).T 
    '''
    plt.xlim([-50, 1400])
    plt.ylim([-300, 800])
    plt.gca().invert_yaxis()
    plt.plot(rfl_x, rfl_y, 'x', color='red', ms=5)
    '''
    #plt.plot(ir_x_list, ir_y_list, 'o', color='orange', ms=3)
    #plt.plot(wc_x_list, wc_y_list, 'o', color='blue', ms=3)
    '''
    plt.plot(ir_corner_x, ir_corner_y, 'x', color='pink', ms=8)
    plt.plot(wc_corner_x, wc_corner_y, 'x', color='green', ms=8)
    '''
    '''
    for i, rec in enumerate(ir_list_a) :
        plt.text(rec[0]+.04, rec[1]+.04, "IR"+str(i), fontsize=8, color='orange')
    for i, rec in enumerate(wc_list_a) :
        plt.text(rec[0]+.04, rec[1]+.04, "WC"+str(i), fontsize=8, color='blue')

    plt.gca().add_patch(Rectangle((0,0),1280,700,linewidth=1,edgecolor='r',facecolor='none'))
    '''
    mean_diff_ir_a = findAvgDiff(rfl_pos, ir_list_a)
    mean_diff_wc_a = findAvgDiff(rfl_pos, wc_list_a)


    line = str(_i) + ',' + str(round(mean_diff_ir_b, 2)) + ',' + str(round(mean_diff_ir_a, 2)) + ',' + str(round(mean_diff_wc_b, 2)) + ',' + str(round(mean_diff_wc_a, 2)) + "\n"

    #f.write(line)


    #plot
    plt.figure()
    plt.title("Infrared-Based Case #"+str(_i+1))
    plt.xlim([-50, 1400])
    plt.ylim([-300, 800])
    plt.gca().invert_yaxis()
    plt.plot(rfl_x, rfl_y, 'x', color='red', ms=5)
    plt.plot(ir_x_list_b, ir_y_list_b, 'x', color='blue', ms=5)
    plt.plot(ir_x_list_a, ir_y_list_a, 'x', color='green', ms=5)
    for i, rec in enumerate(ir_list_b) :
        plt.text(rec[0]+.04, rec[1]+.04, "P"+str(i), fontsize=8, color='blue')
    for i, rec in enumerate(ir_list_a) :
        plt.text(rec[0]+.04, rec[1]+.04, "P"+str(i)+'\'', fontsize=8, color='green')

    plt.gca().add_patch(Rectangle((0,0),1280,700,linewidth=1,edgecolor='r',facecolor='none'))
    plt.savefig("results/deteriorate/Case"+str(_i+1)+"_0_ir.png")

    plt.figure()
    plt.title("Webcam-Based Case #"+str(_i+1))
    plt.xlim([-50, 1400])
    plt.ylim([-300, 800])
    plt.gca().invert_yaxis()
    plt.plot(rfl_x, rfl_y, 'x', color='red', ms=5)
    plt.plot(wc_x_list_b, wc_y_list_b, 'x', color='blue', ms=5)
    plt.plot(wc_x_list_a, wc_y_list_a, 'x', color='green', ms=5)
    for i, rec in enumerate(wc_list_b) :
        plt.text(rec[0]+.04, rec[1]+.04, "P"+str(i), fontsize=8, color='blue')
    for i, rec in enumerate(wc_list_a) :
        plt.text(rec[0]+.04, rec[1]+.04, "P"+str(i)+'\'', fontsize=8, color='green')

    plt.gca().add_patch(Rectangle((0,0),1280,700,linewidth=1,edgecolor='r',facecolor='none'))
    plt.savefig("results/deteriorate/Case"+str(_i+1)+"_1_wc.png")

    #plt.show()

#f.close()


