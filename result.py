import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
import numpy as np
import json


def pointavg (data, scope) :
    N = 0
    sum_x = 0
    sum_y = 0
    for point in data :
        if point[0] > scope['minX'] and point[0] < scope['maxX'] and point[1] > scope['minY'] and point[1] < scope['maxY'] :
            sum_x += point[0]
            sum_y += point[1]
            N += 1

    avg_x = int(sum_x / N)
    avg_y = int(sum_y / N)
    return [avg_x, avg_y]

def pointmedian (data, scope) :
    _data_x = []
    _data_y = []
    for point in data :
        if point[0] > scope['minX'] and point[0] < scope['maxX'] and point[1] > scope['minY'] and point[1] < scope['maxY'] :
            _data_x.append(point[0])
            _data_y.append(point[1])
    median_x = statistics.median(_data_x)
    median_y = statistics.median(_data_y)
    return [median_x, median_y]


def rev_calc_corner (center, q_avg, corner) :
    distance_avg_x = q_avg[0] - center[0]
    distance_corner_x = corner[0] - center[0]
    ratio_x = distance_corner_x / distance_avg_x

    distance_avg_y = q_avg[1] - center[1]
    distance_corner_y = corner[1] - center[1]
    ratio_y = distance_corner_y / distance_avg_y

    return [round(ratio_x, 2), round(ratio_y, 2)]

def calc_corner (center, q_avg, param) :
    distance_avg_x = q_avg[0] - center[0]
    corner_x = center[0] + (distance_avg_x * param[0])

    distance_avg_y = q_avg[1] - center[1]
    corner_y = center[1] + (distance_avg_y * param[1])
    return [int(corner_x), int(corner_y)]


def mapping (x, y, ref_corner):
    point = interpolate(ref_corner, x, y)
    web_screen = [1280, 700]
    return [ int(web_screen[0]*point[0]) , int(web_screen[1]*point[1]) ]


def interpolate (corner, x, y):
    A=np.array([[1, 0, 0, 0], [1, 1, 0, 0], [1, 1, 1, 1], [1, 0, 1, 0]])
    AI = np.linalg.inv(A)

    px = np.array([[corner[0][0], corner[1][0], corner[2][0], corner[3][0]]])
    py = np.array([[corner[0][1], corner[1][1], corner[2][1], corner[3][1]]])

    a = np.matmul(AI,px.transpose())
    b = np.matmul(AI,py.transpose())

    y_ = mapping2(x,y,a,b)

    px = np.array([[corner[0][1], corner[3][1], corner[2][1], corner[1][1]]])
    py = np.array([[corner[0][0], corner[3][0], corner[2][0], corner[1][0]]])

    a = np.matmul(AI,px.transpose())
    b = np.matmul(AI,py.transpose())

    x_ = mapping2(y,x,a,b)

    return [x_, y_]

def mapping2(x,y,a,b):
    aa = a[3]*b[2] - a[2]*b[3]
    bb = a[3]*b[0] -a[0]*b[3] + a[1]*b[2] - a[2]*b[1] + x*b[3] - y*a[3]
    cc = a[1]*b[0] -a[0]*b[1] + x*b[1] - y*a[1]

    det = np.sqrt(bb*bb - 4*aa*cc)
    m = (-bb+det)/(2*aa)

    return m

def serialize_txt (_list) :
    txt = ""
    for data in _list :
        if txt != "" :
            txt += ","
        txt += str(data)
    return txt

def w_average (_list) :
    list_len = len(_list)
    sum_ = 0
    N = 0
    for i in _list :
        sum_ += i*list_len
        N += list_len
        list_len -= 1
    return int(sum_ / N)

def calc_refcorner (x_, y_) :
    a_x = w_average([x_[0], x_[1], x_[2], x_[3]]) - 60
    a_y = w_average([y_[0], y_[4], y_[8], y_[12], y_[16]]) - 60

    b_x = w_average([x_[16], x_[17], x_[18], x_[19]]) + 60
    b_y = w_average([y_[16], y_[12], y_[8], y_[4], y_[0]]) - 60

    c_x = w_average([x_[19], x_[18], x_[17], x_[16]]) + 60
    c_y = w_average([y_[19], y_[15], y_[11], y_[7], y_[3]]) + 60

    d_x = w_average([x_[3], x_[2], x_[1], x_[0]]) - 60
    d_y = w_average([y_[3], y_[7], y_[11], y_[15], y_[19]]) + 60

    return [[a_x, a_y], [b_x, b_y], [c_x, c_y], [d_x, d_y]]

ref_corner = [[-51, -205], [1148, -191], [1166, 580], [-56, 499]]

RFL_Positions = [[60,60],[60,253],[60,446],[60,639],[350,60],[350,253],[350,446],[350,639],[640,60],[640,253],[640,446],[640,639],[930,60],[930,253],[930,446],[930,639],[1220,58],[1220,253],[1220,446],[1220,639]]



for i_ in range(0,20) :


    p = 'p'+str(i_)

    json_raw = open('results/'+p+'.json', 'r').read()
    json_raw_arr = json.loads(json_raw)





    plt.figure()
    plt.title(p)

    json_arr = json_raw_arr['gaze_result']

    data_tobii = json_arr['tobii']
    _data_tobii = []

    #params_ir = [[3.24, 5.31], [3.52, 4.4], [3.91, 5.37], [3.81, 5.09]] #ir
    params_ir = [[4.19, 5.53], [4.68, 5.56], [4.84, 5.42], [4.27, 5.21]] #ir
    #params_wc = [[4.08, 5.52], [3.71, 4.89], [4.28, 5.19], [4.04, 6.51]] #webcam
    #params_wc = [[3.55, 5.38], [3.7, 4.39], [3.47, 5.11], [4.05, 5.46]] #webcam2
    params_wc = [[3.7, 4.79], [4.21, 4.43], [4.74, 4.61], [4.96, 4.17]] #webcam

    start = 13
    span = 7
    duration = 20

    for i, rec in enumerate(data_tobii) :
        if i > (len(data_tobii)/duration) * start and i < ((len(data_tobii)/duration) * start) + span*(len(data_tobii)/duration) :
            _data_tobii.append(rec)

    data_webcam = json_arr['webcam']
    _data_webcam = []

    for i, rec in enumerate(data_webcam) :
        if i > (len(data_webcam)/duration) * start and i < ((len(data_webcam)/duration) * start) + span*(len(data_webcam)/duration) :
            _data_webcam.append(rec)

    data2 = np.array(_data_tobii)
    x, y = data2.T

    data4 = np.array(_data_webcam)
    x2, y2 = data4.T

    data_frame = _data_tobii
    center = pointavg(data_frame, {'minX':-9999 , 'maxX':9999 ,'minY':-9999 ,'maxY':9999 })
    q1 = pointavg(data_frame, {'minX':-9999 , 'maxX':center[0] ,'minY':-9999 ,'maxY':center[1] })
    q2 = pointavg(data_frame, {'minX':center[0] , 'maxX':9999 ,'minY':-9999 ,'maxY':center[1] })
    q3 = pointavg(data_frame, {'minX':-9999 , 'maxX':center[0] ,'minY':center[1] ,'maxY':9999 })
    q4 = pointavg(data_frame, {'minX':center[0] , 'maxX':9999 ,'minY':center[1] ,'maxY':9999 })

    corner_q1 = calc_corner (center, q1, params_ir[0])
    corner_q2 = calc_corner (center, q2, params_ir[1])
    corner_q3 = calc_corner (center, q3, params_ir[2])
    corner_q4 = calc_corner (center, q4, params_ir[3])

    ref_corner_ir = [corner_q1, corner_q2, corner_q4, corner_q3]
    ref_corner_ir_x, ref_corner_ir_y = np.array(ref_corner_ir).T

    data_frame = _data_webcam
    center = pointavg(data_frame, {'minX':-9999 , 'maxX':9999 ,'minY':-9999 ,'maxY':9999 })
    q1 = pointavg(data_frame, {'minX':-9999 , 'maxX':center[0] ,'minY':-9999 ,'maxY':center[1] })
    q2 = pointavg(data_frame, {'minX':center[0] , 'maxX':9999 ,'minY':-9999 ,'maxY':center[1] })
    q3 = pointavg(data_frame, {'minX':-9999 , 'maxX':center[0] ,'minY':center[1] ,'maxY':9999 })
    q4 = pointavg(data_frame, {'minX':center[0] , 'maxX':9999 ,'minY':center[1] ,'maxY':9999 })

    corner_q1 = calc_corner (center, q1, params_wc[0])
    corner_q2 = calc_corner (center, q2, params_wc[1])
    corner_q3 = calc_corner (center, q3, params_wc[2])
    corner_q4 = calc_corner (center, q4, params_wc[3])

    ref_corner_wc = [corner_q1, corner_q2, corner_q4, corner_q3]
    ref_corner_wc_x, ref_corner_wc_y = np.array(ref_corner_wc).T

    plt.xlim([-500, 1800])
    plt.ylim([-500, 1500])
    plt.gca().invert_yaxis()
    plt.plot(x, y, 'o', color='orange', ms=1)
    plt.plot(x2, y2, 'o', color='blue', ms=1)

    plt.plot(ref_corner_ir_x, ref_corner_ir_y, 'x', color='pink', ms=8)
    plt.plot(ref_corner_wc_x, ref_corner_wc_y, 'x', color='green', ms=8)

    plt.gca().add_patch(Rectangle((0,0),1280,700,linewidth=1,edgecolor='r',facecolor='none'))







plt.show()


