<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="src/js/jquery.js"></script>
<script src="src/js/webgazer.js"></script>
</head>
<script>

var gaze_wc = [0,0];
var gaze_tobii = [0,0];
function getGazeData (tracker){
    if(tracker == 'webcam')
        return gaze_wc;
    else if(tracker == 'tobii')
        return gaze_tobii;
}


var gaze_result = {
    'webcam' : [],
    'tobii' : []
};
function record_freelooking (callback){
    $('#rfl_focus').css({display: 'none'});
    $('#rfl_pre').css({display: 'none'});
    play_video();

    var itv = setInterval(function(){
        gaze_result['webcam'].push([gaze_wc[0], gaze_wc[1]]);
        gaze_result['tobii'].push([gaze_tobii[0], gaze_tobii[1]]);
    }, 50);

    setTimeout(function(){
        stop_video();
        clearInterval(itv);
        callback();
    }, 60*20*1000);

}

var RFL_results_pre = [];
var RFL_results_post = [];
//x : 60 350 640 930 1220 
//y : 60 253 446 639 
//var RFL_Positions = [[80,65],[80,230],[80,430]];
var RFL_Positions = [[60,60],[60,253],[60,446],[60,639],[350,60],[350,253],[350,446],[350,639],[640,60],[640,253],[640,446],[640,639],[930,60],[930,253],[930,446],[930,639],[1220,58],[1220,253],[1220,446],[1220,639]];
var rfl_interval = false;
var rfl_record_trigger = false;
var rfl_record_mode = 'pre';
var rfl_pos_pointer = 0;

function record_RFL (mode, callback){
    rfl_record_mode = mode;
    showRFLscreen (0, callback);
}

function play_video (){
    $('#bt1').css('display', 'none');
    $('#video1').css('display', 'inherit');
    var myVideo = document.getElementById("video1"); 
    var times = [0, 1200, 2400, 3600, 4800, 6000];
    myVideo.currentTime = times[2];
    myVideo.play(); 

    setTimeout(function(){ $('.rfl').css('display', 'none'); }, 10000);
}

function stop_video (){
    var myVideo = document.getElementById("video1"); 
    myVideo.pause();
    $('#video1').css('display', 'none');
}

function showRFLscreen (pos, callback){
    if(RFL_Positions.length > pos){
        rfl_pos_pointer = pos;
        $('#rfl_focus').css({display: 'none'});
        $('#rfl_pre').css({left: RFL_Positions[pos][0]-15, top: RFL_Positions[pos][1]-15 });
        $('#rfl_pre').css({display: 'inherit'});
        setTimeout(function(){
            //hide pre
            $('#rfl_pre').css({display: 'none'});
            $('#rfl_focus').css({left: RFL_Positions[pos][0]-15, top: RFL_Positions[pos][1]-15 });
            $('#rfl_focus').css({display: 'inherit'});
            
            rfl_record_trigger = true;
            setTimeout(() => {
                rfl_record_trigger = false;
                showRFLscreen(pos+1, callback);
            }, 2000);
            
        }, 3000);
    }else{
        callback();
    }
}

var finaldata = {};
function start_testing (){
    
    record_RFL('pre', function(){
        show_RFL_result(function(){
            record_freelooking(function(){
                /*setTimeout(() => {
                    $('#tobii_dot').css('display', 'none');
                    $('#wc_dot').css('display', 'none');
                }, 60000);*/
                record_RFL('post', function(){
                    finaldata['rfl_pre'] = RFL_results_pre;
                    finaldata['rfl_post'] = RFL_results_post;
                    finaldata['gaze_result'] = gaze_result;
                    alert("End."); 
                });
            });
        });
    });
}

var IR_x = {}, IR_y = {}, WC_x = {}, WC_y = {};
const avg = arr => arr.reduce( ( p, c ) => p + c, 0 ) / arr.length;
function show_RFL_result (callback){
    var result_raw = []; // [[[0,0], [1,1], [2,2]], [[0,0], [1,1], [2,2]] ];
    for(i=0; i<RFL_results_pre.length; i++){
        var pos = RFL_results_pre[i][0];
        
        if(IR_x[pos] == undefined){
            IR_x[pos] = [];
            IR_y[pos] = [];
            WC_x[pos] = [];
            WC_y[pos] = [];
        }
        IR_x[pos].push(RFL_results_pre[i][2][0]);
        IR_y[pos].push(RFL_results_pre[i][2][1]);
        WC_x[pos].push(RFL_results_pre[i][1][0]);
        WC_y[pos].push(RFL_results_pre[i][1][1]);
    }

    for (const [key, value] of Object.entries(IR_x)) {
        var wc_x = avg(WC_x[key]);
        var wc_y = avg(WC_y[key]);
        var ir_x = avg(IR_x[key]);
        var ir_y = avg(IR_y[key]);
        result_raw.push([ RFL_Positions[key], [wc_x, wc_y], [ir_x, ir_y] ]);
    }

    console.log(result_raw);

    for(i=0 ; i<result_raw.length ;i++){
        $('body').append('<div class="rfl dot" style="background-color: red; left: '+(result_raw[i][0][0])+'px; top: '+(result_raw[i][0][1])+'px"></div>');
        $('body').append('<div class="rfl dot" style="background-color: orange; left: '+(result_raw[i][1][0])+'px; top: '+(result_raw[i][1][1])+'px"></div>');
        $('body').append('<div class="rfl label" style="color: orange; left: '+(result_raw[i][1][0])+'px; top: '+((result_raw[i][1][1]) - 20)+'px">WC'+i+'</div>');
        $('body').append('<div class="rfl dot" style="background-color: blue; left: '+(result_raw[i][2][0])+'px; top: '+(result_raw[i][2][1])+'px"></div>');
        $('body').append('<div class="rfl label" style="color: blue; left: '+(result_raw[i][2][0])+'px; top: '+((result_raw[i][2][1]) - 20)+'px">IR'+i+'</div>');
    }

    setTimeout(function(){
        $('.rlf').remove();
        callback();
    }, 20*1000);
}

function save_data (){
    
}

var SmoothQueue = function (queueSize=5){
	this.limit = queueSize;
	this.head = 0;
	this.weight = 0;
	this.data = [];
}

SmoothQueue.prototype.push = function (data){
	this.data[this.head] = data;
	this.head++;
	if(this.head >= this.limit){
		this.head = 0;
	}
	if(this.weight < this.limit)
		this.weight++;
}

SmoothQueue.prototype.avarage = function (){
	var sum = 0;
	for(var i=0; i<this.weight; i++){
		sum += this.data[i];
	}
	return (sum / this.weight);
}

var smooth_q = {
	'gaze_x' : new SmoothQueue(30),
	'gaze_y' : new SmoothQueue(30)
}	

var rfl_ = [];
$(document).ready(function(){
    /*$('body').click(function(e){
        if(rfl_.length < 20){
            alert(e.pageX + ' ' + e.pageY);
            rfl_.push([e.pageX, e.pageY]);    
        }
    });*/

    rfl_interval = setInterval(function(){
        if(rfl_record_trigger == true){
            if(rfl_record_mode == 'pre'){
                RFL_results_pre.push([rfl_pos_pointer, [gaze_wc[0], gaze_wc[1]], [gaze_tobii[0], gaze_tobii[1]]]);    
            }else{
                RFL_results_post.push([rfl_pos_pointer, [gaze_wc[0], gaze_wc[1]], [gaze_tobii[0], gaze_tobii[1]]]);   
            }
        }
    }, 50);

    setInterval(function(){
        $('#tobii_dot').css({left: gaze_tobii[0]-3, top: gaze_tobii[1]-3 });
        $('#wc_dot').css({left: gaze_wc[0]-3, top: gaze_wc[1]-3 });
    }, 45);

    var native_screen = [1920, 1080];
    var web_screen = [1280, 800];
    var ws = new WebSocket("ws://127.0.0.1:6789/");
    ws.onmessage = function (event) {
        var pos = JSON.parse(event.data);
        //console.log(pos.x + ',' + pos.y + ',' + pos.valid);
        
        if(pos.valid == 'valid'){
            gaze_tobii[0] = parseInt( parseInt(pos.x) * web_screen[0] / native_screen[0] );
            gaze_tobii[1] = parseInt( parseInt(pos.y) * web_screen[1] / native_screen[1] );
        }
    };

    webgazer.setRegression('ridge') 
    .setTracker('clmtrackr')
    .setGazeListener(function(data, clock) {
        try{
            smooth_q.gaze_x.push(data.x);
            smooth_q.gaze_y.push(data.y);
            gaze_wc[0] = parseInt( smooth_q.gaze_x.avarage() );
            gaze_wc[1] = parseInt( smooth_q.gaze_y.avarage() );
        }catch(e){}
    })
    .begin()
    .showPredictionPoints(false); 
});
</script>
<style>
    .dot{
        width: 6px;
        height: 6px;
        position: absolute;
    }

    .label {
        position: absolute;
    }

    video {
        width: 1280px;
        height: 700px;
        object-fit: cover;
    }
</style>

<body>
    <div class="dot" style="background: #fc0;" id="tobii_dot"></div>
    <div class="dot" style="background: #0cf;" id="wc_dot"></div>
    <img src="rfl_pre.png" id="rfl_pre" style="display: none; position: absolute; width: 30px;">
    <img src="rfl_focus.png" id="rfl_focus" style="display: none; position: absolute;  width: 30px;">
    <button id="bt1" style="position: absolute; top: 300px; left: 600px;" onclick="start_testing();">Start test</button>
    <video id="video1" width="1280" height="700" style="display:none;">
        <source src="video3.mp4" type="video/mp4">
        Your browser does not support HTML video.
    </video>
</body>
</html>
